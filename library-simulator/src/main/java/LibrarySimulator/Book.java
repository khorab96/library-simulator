package LibrarySimulator;

import java.util.Objects;

public class Book extends Item {

    private String author;

    public Book ( String author, String title ) {
        super ( title );
        this.author = author;
    }

    public String getAuthor ( ) {
        return author;
    }

    public void setAuthor ( String author ) {
        this.author = author;

    }

    @Override
    public boolean equals ( Object o ) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals ( o )) return false;
        Book book = (Book) o;
        return Objects.equals ( author, book.author );
    }

    @Override
    public int hashCode ( ) {
        return Objects.hash ( super.hashCode ( ), author );
    }
}

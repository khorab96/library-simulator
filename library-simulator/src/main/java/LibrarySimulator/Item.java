package LibrarySimulator;

import java.util.Objects;

public abstract class Item {

    String title;

    public Item ( String title ) {
        this.title = title;
    }

    public String getTitle ( ) {
        return title;
    }

    public void setTitle ( String title ) {
        this.title = title;

    }

    @Override
    public boolean equals ( Object o ) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals ( title, item.title );
    }

    @Override
    public int hashCode ( ) {
        return Objects.hash ( title );
    }

}

package LibrarySimulator;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Library {


    private Integer currentUserId;
    private Map<Integer, User> usersMap;
    private Map<Item, Integer> itemsMap;
    private Map<Item, ArrayList<User>> itemsUsers;
    private Map<Integer, ArrayList<Item>> usersItems;
    private final static int itemTitlePosition = 0;
    private final static int itemAuthorPosition = 1;
    private final static int itemNumberPosition = 2;
    private final static int itemSignPosition = 3;
    private final static String bookSign = "B";
    private final static String magazineSign = "M";

    public Library ( ) {
        this.currentUserId = 0;
        this.usersMap = new HashMap<Integer, User> ( );
        this.itemsMap = new HashMap<Item, Integer> ( );
        this.itemsUsers = new HashMap<Item, ArrayList<User>> ( );
        this.usersItems = new HashMap<Integer, ArrayList<Item>> ( );
    }

    private Integer generateUserId ( ) {
        return ++this.currentUserId;
    }

    public void addUserToLibrary ( User... users ) {
        for (User user : users) {
            if (!usersMap.containsKey ( user.getUserId ( ) )) {
                Integer newId = this.generateUserId ( );
                user.setUserId ( newId );
                usersMap.put ( newId, user );
            }
        }
    }

    public void printListOfUsers ( ) {
        for (Map.Entry<Integer, User> entry : usersMap.entrySet ( )) {
            User user = entry.getValue ( );
            String userType = "";
            if (user instanceof Student) {
                userType = "S";
            } else if (user instanceof Lecturer) {
                userType = "L";
            }
            System.out.println ( user.getFirstName ( ) + ";" + user.getLastName ( ) + ";" + user.getUserId ( ) + ";" + userType );
        }
    }


    public void addItemToLibrary ( Item... items ) {
        for (Item item : items) {
            if (itemsMap.containsKey ( item )) {
                itemsMap.put ( item, itemsMap.get ( item ) + 1 );
            } else {
                itemsMap.put ( item, 1 );
            }
        }
    }

    public boolean rentItemToUser ( Item item, User user ) {
        if (!usersMap.containsKey ( user.getUserId ( ) ))
            return false;
        ArrayList<Item> itemsRented = usersItems.get ( user.getUserId ( ) );
        if (itemsRented == null)
            itemsRented = new ArrayList<Item> ( );
        if (itemsRented.size ( ) >= user.getLiteratureToBorrow ( ))
            return false;

        int allItems = itemsMap.get ( item );
        ArrayList<User> usersWhoRented = itemsUsers.get ( item );
        if (usersWhoRented == null)
            usersWhoRented = new ArrayList<User> ( );
        if (usersWhoRented.size ( ) >= allItems)
            return false;

        itemsRented.add ( item );
        usersItems.put ( user.getUserId ( ), itemsRented );

        usersWhoRented.add ( user );
        itemsUsers.put ( item, usersWhoRented );

        return true;
    }

    public void printListOfMagazines ( ) {
        for (Map.Entry<Item, Integer> entry : itemsMap.entrySet ( )) {
            Item item = entry.getKey ( );
            if (item instanceof Magazine) {
                Magazine magazine = (Magazine) item;
                int allItems = itemsMap.get ( item );
                ArrayList<User> usersWhoRented = itemsUsers.get ( item );
                int usersWhoRentedCount = 0;
                if (usersWhoRented != null)
                    usersWhoRentedCount = usersWhoRented.size ( );
                System.out.println ( magazine.getTitle ( ) + ";" + magazine.getNumber ( ) + ";" + allItems + ";" + (allItems - usersWhoRentedCount) );
            }
        }
    }

    public void printListBooks ( ) {
        for (Map.Entry<Item, Integer> entry : itemsMap.entrySet ( )) {
            Item item = entry.getKey ( );
            if (item instanceof Book) {
                Book book = (Book) item;
                int allItems = itemsMap.get ( item );
                ArrayList<User> usersWhoRented = itemsUsers.get ( item );
                int usersWhoRentedCount = 0;
                if (usersWhoRented != null)
                    usersWhoRentedCount = usersWhoRented.size ( );
                System.out.println ( book.getTitle ( ) + ";" + book.getAuthor ( ) + ";" + allItems + ";" + (allItems - usersWhoRentedCount) );
            }
        }
    }


    public void importItemsFromFile ( String csvFile ) {
        try (BufferedReader br = new BufferedReader ( new FileReader ( csvFile ) )) {
            String line;
            while ((line = br.readLine ( )) != null) {
                String[] values = line.split ( ";" );
                int numberOfItems = Integer.parseInt ( values[itemNumberPosition] );
                if (values[itemSignPosition].equalsIgnoreCase ( bookSign )) {
                    for (int i = 0; i < numberOfItems; i++) {
                        Book book = new Book ( values[itemAuthorPosition], values[itemTitlePosition] );
                        this.addItemToLibrary ( book );
                    }
                } else if (values[itemSignPosition].equalsIgnoreCase ( magazineSign )) {
                    for (int i = 0; i < numberOfItems; i++) {
                        Magazine magazine = new Magazine ( values[itemAuthorPosition], values[itemTitlePosition] );
                        this.addItemToLibrary ( magazine );
                    }
                }
            }
        } catch (IOException e) {
            System.out.println ( e.getMessage ( ) );
        } catch (NumberFormatException e) {
            System.out.println ( e.getMessage ( ) );
        }
    }

    public void exportUsersWithItemsToFile ( String csvFile ) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter ( csvFile, "UTF-8" );

            for (Map.Entry<Integer, ArrayList<Item>> entry : usersItems.entrySet ( )) {
                String str = entry.getKey ( ) + "[";
                ArrayList<Item> items = entry.getValue ( );
                for (int i = 0; i < items.size ( ); i++) {
                    Item item = items.get ( i );
                    if (item instanceof Book) {
                        str += item.getTitle ( ) + "-" + ((Book) item).getAuthor ( );
                    } else if (item instanceof Magazine) {
                        str += item.getTitle ( ) + "-" + ((Magazine) item).getNumber ( );
                    }
                    if (i == items.size ( ) - 1)
                        str += "]";
                    else
                        str += ";";
                }
                writer.println ( str );
            }
        } catch (FileNotFoundException e) {
            System.out.println ( e.getMessage ( ) );
        } catch (UnsupportedEncodingException e) {
            System.out.println ( e.getMessage ( ) );
        } finally {
            writer.close ( );
        }
    }


}

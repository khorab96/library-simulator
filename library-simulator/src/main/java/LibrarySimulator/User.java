package LibrarySimulator;

public abstract class User {

    private String firstName;
    private String lastName;
    private Integer userId;
    private Integer literatureToBorrow;


    public User ( String firstName, String lastName, Integer literatureToBorrow ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.literatureToBorrow = literatureToBorrow;
    }

    public String getFirstName ( ) {
        return firstName;
    }

    public void setFirstName ( String firstName ) {
        this.firstName = firstName;
    }

    public String getLastName ( ) {
        return lastName;
    }

    public void setLastName ( String lastName ) {
        this.lastName = lastName;
    }

    public Integer getUserId ( ) {
        return userId;
    }

    public void setUserId ( Integer userId ) {
        this.userId = userId;
    }

    public int getLiteratureToBorrow ( ) {
        return literatureToBorrow;
    }
}
